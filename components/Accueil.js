import React from "react";
import { StyleSheet, View, Button } from "react-native";
import TitleAppli from "./TitleAppli";

class Accueil extends React.Component {
  constructor(props) {
    super(props);
  }

  _goToMixDoggo = () => {
    this.props.navigation.navigate("DoggoListMale");
  };

  _goToCreateMix = () => {
    this.props.navigation.navigate("DoggoNewMixMale");
  };

  _goToCreateDoggo = () => {
    this.props.navigation.navigate("DoggoCreation");
  };

  render() {
    return (
      <View style={styles.container}>
        <TitleAppli></TitleAppli>

        <View style={styles.buttonsContainer}>
          <View style={styles.mixButton}>
            <Button
              title="Mix some Doggos !"
              onPress={() => this._goToMixDoggo()}
            ></Button>
          </View>
          <View style={styles.newMixButton}>
            <Button
              title="Create new Doggo mix"
              onPress={() => this._goToCreateMix()}
            ></Button>
          </View>
          <View style={styles.newDoggobutton}>
            <Button
              title="Create a new Doggo race !"
              onPress={() => this._goToCreateDoggo()}
            ></Button>
          </View>
        </View>

        <View style={styles.emptyContainer}></View>
      </View>
    );
  }
}
export default Accueil;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: "center",
    justifyContent: "center",
  },
  buttonsContainer: {
    flex: 9,
    justifyContent: "center",
  },
  mixButton: {
    justifyContent: "center",
    paddingBottom: 10,
    marginLeft: 70,
    marginRight: 70,
    marginBottom: 40,
  },
  newMixButton: {
    justifyContent: "center",
    paddingBottom: 10,
    paddingTop: 10,
    marginLeft: 70,
    marginRight: 70,
    marginTop: 10,
    marginBottom: 40,
  },
  newDoggobutton: {
    justifyContent: "center",
    paddingTop: 10,
    marginLeft: 70,
    marginRight: 70,
    marginTop: 10,
  },
  emptyContainer: {
    flex: 2,
  },
});
