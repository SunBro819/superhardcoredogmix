import React from "react";
import { View, Text, StyleSheet, Image, Button } from "react-native";

class DoggoItemFemale extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isDetailsDisplayed: false };
  }

  _toggleDisplayDetails() {
    this.setState({ isDetailsDisplayed: !this.state.isDetailsDisplayed });
  }

  _displayDetails(description, couleur, caractere) {
    if (this.state.isDetailsDisplayed) {
      return (
        <View>
          <View style={styles.doggoDesc}>
            <Text style={styles.doggoDescText}>{description} </Text>
          </View>
          <View style={styles.doggoProprietes}>
            <View style={styles.doggoCouleur}>
              <Text style={styles.doggoCouleurText}>{couleur}</Text>
            </View>
            <View style={styles.doggoCarac}>
              <Text style={styles.doggoCaracText}>{caractere}</Text>
            </View>
          </View>
        </View>
      );
    }
  }

  render() {
    const chienne = this.props.chienne;
    const maleId = this.props.maleId;
    const goToResult = this.props.goToResult;

    return (
      <View>
        <View style={styles.doggoItem}>
          <Image
            style={styles.image}
            source={{ uri: chienne.imageUrl }}
          ></Image>
          <View style={styles.doggoInfo}>
            <View style={styles.doggoName}>
              <Text style={styles.doggoNameText}>{chienne.name}</Text>
            </View>
            <View style={styles.doggoInfoButton}>
              <Button
                title="Select this doggo"
                onPress={() => goToResult(maleId, chienne._id)}
              />
            </View>
            <View style={styles.doggoInfoButton}>
              <Button
                title="+ INFOS"
                onPress={() => this._toggleDisplayDetails()}
              />
            </View>
          </View>
        </View>
        <View style={styles.doggoOtherInfos}>
          {this._displayDetails(chienne.desc, chienne.color, chienne.character)}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  doggoItem: {
    flexDirection: "row",
    backgroundColor: "#e3e3e3",
    margin: 5,
  },
  image: {
    width: 120,
    height: 120,
    margin: 5,
    backgroundColor: "gray",
  },
  doggoInfo: {
    flex: 1,
    margin: 2,
    flexDirection: "column",
  },
  doggoOtherInfos: {
    flex: 1,
    padding: 5,
  },
  doggoName: {
    flex: 1,
    justifyContent: "center",
  },
  doggoNameText: {
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
  },
  doggoInfoButton: {
    paddingBottom: 10,
  },
  doggoDesc: {
    flex: 2,
    backgroundColor: "#e3e3e3",
    margin: 5,
  },
  doggoDescText: {
    color: "gray",
    padding: 5,
  },
  doggoProprietes: {
    flex: 2,
    flexDirection: "row",
  },
  doggoCouleur: {
    flex: 1,
    color: "gray",
    backgroundColor: "#e3e3e3",
    margin: 5,
  },
  doggoCarac: {
    flex: 1,
    backgroundColor: "#e3e3e3",
    margin: 5,
  },
  doggoCouleurText: {
    color: "gray",
    padding: 5,
  },
  doggoCaracText: {
    color: "gray",
    padding: 5,
  },
});
export default DoggoItemFemale;
