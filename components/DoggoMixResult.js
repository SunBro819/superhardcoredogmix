import React from "react";
import { StyleSheet, View, Image, Text, Button } from "react-native";
import { getDoggosMix } from "../api/DoggoAPI.js";

class DoggoMixResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = { doggo: {} };
  }

  componentDidMount() {
    getDoggosMix(
      this.props.navigation.state.params.maleId,
      this.props.navigation.state.params.femaleId
    )
      .then((res) => {
        this.state.doggo = res.data;
        this.forceUpdate();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  _goToAccueil = () => {
    this.props.navigation.navigate("Accueil");
  };

  _goToMixDoggoAgain = () => {
    this.props.navigation.navigate("DoggoListMale");
  };

  render() {
    const resultDoggo = this.state.doggo;

    return (
      <View style={styles.container}>
        <View style={styles.headlineContainer}>
          <Text style={styles.headline}>Superhardcoredoggomix RESULT !</Text>
        </View>
        <View style={styles.resultContainer}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={{ uri: resultDoggo.imageUrl }}
            ></Image>
          </View>
          <View style={styles.nameContainer}>
            <Text style={styles.doggoNameText}>{resultDoggo.name}</Text>
          </View>
        </View>
        <View style={styles.buttonsContainer}>
          <View style={styles.accueilButton}>
            <Button
              title="Accueil"
              onPress={() => this._goToAccueil()}
            ></Button>
          </View>
          <View style={styles.newMixButton}>
            <Button
              title="Recommencer"
              onPress={() => this._goToMixDoggoAgain()}
            ></Button>
          </View>
        </View>
      </View>
    );
  }
}
export default DoggoMixResult;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: "center",
    justifyContent: "center",
  },
  headlineContainer: { flex: 1, justifyContent: "flex-end" },
  headline: {
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 10,
    paddingBottom: 30,
  },
  resultContainer: {
    flex: 1.5,
    flexDirection: "row",
    backgroundColor: "#e3e3e3",
    margin: 10,
  },
  imageContainer: {
    flex: 1,
    margin: 10,
    alignContent: "center",
    justifyContent: "center",
  },
  image: {
    width: 150,
    height: 150,
    margin: 10,
    backgroundColor: "grey",
  },
  nameContainer: {
    flex: 1,
    margin: 10,
    justifyContent: "center",
  },
  doggoNameText: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
  buttonsContainer: {
    flex: 2,
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "flex-end",
    padding: 10,
    margin: 20,
  },
  accueilButton: {
    flex: 1,
    justifyContent: "center",
    marginRight: 10,
  },
  newMixButton: {
    flex: 1,
    justifyContent: "center",
    marginLeft: 10,
  },
});
