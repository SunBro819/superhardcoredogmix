import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
// import chiens from "../helpers/chiensData.js";
import DoggoItemMale from "./DoggoItemMale.js";
import { getDoggos } from "../api/DoggoAPI.js";

class DoggoListMale extends React.Component {
  constructor(props) {
    super(props);
    this.state = { doggo: [] };
  }

  componentDidMount() {
    getDoggos()
      .then((res) => {
        this.state.doggo = res.data;
        this.forceUpdate();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  _goToFemale = (idDoggo) => {
    this.props.navigation.navigate("DoggoListFemale", { maleId: idDoggo });
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.doggo}
          keyExtractor={(item) => item._id}
          renderItem={({ item }) => (
            <DoggoItemMale chien={item} goToFemale={this._goToFemale} />
          )}
        />
      </View>
    );
  }
}
export default DoggoListMale;

const styles = StyleSheet.create({
  container: {
    flex: 7,
    alignContent: "center",
    justifyContent: "center",
  },
});
