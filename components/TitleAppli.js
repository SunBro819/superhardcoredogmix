import React from "react";
import { StyleSheet, View, Text } from "react-native";

class TitleAppli extends React.Component {
  render() {
    return (
      <View style={styles.titleBlock}>
        {/* Title */}
        <Text style={styles.title}>Super Hardcore Chien Mix</Text>

        {/* Subtitle */}
        <Text style={styles.subtitle}>
          L'application pour mixer les toutous
        </Text>
        <Text style={styles.subtitle}>(sponsorisée par M.TORGUE !!!)</Text>
      </View>
    );
  }
}
export default TitleAppli;

const styles = StyleSheet.create({
  titleBlock: {
    flex: 2,
    backgroundColor: "#4b7bec",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    color: "white",
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  subtitle: {
    color: "white",
    fontSize: 11,
    textAlign: "center",
  },
});
