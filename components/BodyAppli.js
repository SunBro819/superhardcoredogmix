import React from "react";
import { StyleSheet, View, Text, Picker, Image } from "react-native";

class BodyAppli extends React.Component {
  render() {
    return (
      <View style={styles.block}>
        {/* Male block */}
        <Text style={styles.blockText}>Male</Text>
        <Picker>
          <Picker.Item value="Labrador" label="Labrador" />
        </Picker>

        {/* Female block */}
        <Text style={styles.blockText}>Female</Text>
        <Picker>
          <Picker.Item value="Labrador" label="Labrador" />
        </Picker>

        {/* Result block */}
        <Text style={styles.blockText}>Résultat du mélange :</Text>
        <Image
          source={{
            uri:
              "http://getmemetemplates.com/wp-content/uploads/2020/01/FB_IMG_1578711116905.jpg",
          }}
          style={{
            width: 250,
            height: 250,
            alignSelf: "center",
            resizeMode: "contain",
          }}
        />
      </View>
    );
  }
}
export default BodyAppli;

const styles = StyleSheet.create({
  block: {
    flex: 1,
  },
  blockText: {
    backgroundColor: "#82ccdd",
    textAlign: "center",
    fontSize: 17,
    fontWeight: "bold",
    paddingTop: 10,
    paddingBottom: 10,
  },
});
