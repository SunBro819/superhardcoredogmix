import React from "react";
import { StyleSheet, View, Text, FlatList } from "react-native";
import DoggoItemFemale from "./DoggoItemFemale.js";
import { getDoggos } from "../api/DoggoAPI.js";

class DoggoListFemale extends React.Component {
  constructor(props) {
    super(props);
    this.state = { doggo: [] };
  }

  componentDidMount() {
    getDoggos()
      .then((res) => {
        this.state.doggo = res.data;
        this.forceUpdate();
      })
      .catch((error) => {
        console.log(error);
      });
  }

  _goToResult = (idDoggoMale, idDoggoFemale) => {
    this.props.navigation.navigate("DoggoMixResult", {
      maleId: idDoggoMale,
      femaleId: idDoggoFemale,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.doggo}
          keyExtractor={(item) => item._id}
          renderItem={({ item }) => (
            <DoggoItemFemale
              chienne={item}
              maleId={this.props.navigation.state.params.maleId}
              goToResult={this._goToResult}
            />
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  text: {
    textAlign: "center",
    fontSize: 18,
    color: "blue",
  },
});

export default DoggoListFemale;
