import axios from "axios";

export function getDoggos() {
  const url = "http://chienmix.ml/api/chiens";
  return new Promise((resolve, reject) => {
    return axios
      .get(url)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export function getDoggosMix(maleId, femaleId) {
  console.log(maleId);
  const url = `http://chienmix.ml/api/melange/${maleId}/${femaleId}`;
  return new Promise((resolve, reject) => {
    return axios
      .get(url)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
