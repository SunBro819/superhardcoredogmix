// helpers/DoggoData.js

const data = [
  {
    id: 1,
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCq0NCtxliLMbKpoCypklHOkZO9TQUsh6BIQ&usqp=CAU",
    name: "Labrador",
    desc:
      "Le retriever du Labrador, plus communément appelé labrador retriever ou plus simplement labrador, est une race de chiens originaire du Royaume-Uni. ",
    color: "Noire, marron ou jaune",
    character: "",
  },
  {
    id: 2,
    imageUrl:
      "https://i.pinimg.com/236x/72/6e/a1/726ea1344781a61b6376a4f9b35691f5.jpg",
    name: "Basset fauve de Bretagne",
    desc:
      "Le basset fauve de Bretagne est une race de chien de chasse originaire de Bretagne. ",
    color: "Fauve unicolore.",
    character: "Sociable, affectueux, équilibré.",
  },
  {
    id: 3,
    imageUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIGHgg7fnP45TK_C3MrUSTJkzqNEomo7tnxw&usqp=CAU",
    name: "Beagle",
    desc:
      "Le beagle  est une race de chien originaire d’Angleterre, de taille petite à moyenne.",
    color:
      "Uni (blanc), bicolore (blanc et sable) ou tricolore (blanc, noir et marron).",
    character: "Amical et attachant.",
  },
];

export default data;
