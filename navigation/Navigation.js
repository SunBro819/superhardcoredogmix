import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import Accueil from "../components/Accueil";
import DoggoListFemale from "../components/DoggoListFemale";
import DoggoListMale from "../components/DoggoListMale";
import DoggoMixResult from "../components/DoggoMixResult";

const DoggoStackNavigator = createStackNavigator({
  Accueil: {
    screen: Accueil,
    navigationOptions: {
      title: "Accueil",
    },
  },
  DoggoListMale: {
    screen: DoggoListMale,
    navigationOptions: {
      title: "Choose a male doggo",
    },
  },
  DoggoListFemale: {
    screen: DoggoListFemale,
    navigationOptions: {
      title: "Choose a female doggo",
    },
  },
  DoggoMixResult: {
    screen: DoggoMixResult,
    navigationOptions: {
      title: "Doggo mix result",
    },
  },
  // DoggoNewMixMale: {
  //   screen: DoggoNewMixMale,
  //   navigationOptions: {
  //     title: "Choose or create a male doggo",
  //   },
  // },
  // DoggoNewMixFemale: {
  //   screen: DoggoNewMixFemale,
  //   navigationOptions: {
  //     title: "Choose or create a female doggo",
  //   },
  // },
  // DoggoNewMix: {
  //   screen: DoggoNewMix,
  //   navigationOptions: {
  //     title: "Create a new mixed doggo",
  //   },
  // },
  // DoggoCreation: {
  //   screen: DoggoCreation,
  //   navigationOptions: {
  //     title: "Create a new mixed doggo",
  //   },
  // },
});
export default createAppContainer(DoggoStackNavigator);
